﻿//========Script JS permettant l'affichage des nappes selon les chaînes récupérées dans la table PDM_OPTI_CALCUL========//


// On récupère les paramètres depuis la vue "PDM/Nappes/_DisplayDetailsNappeDrawPartial" 
function drawNappes(divID, maximumValue, LValue, nbNappesCruise) {
    const idNappe = divID.id;
    const string = divID.innerHTML;
    const selectNappeString = document.getElementById(idNappe);
    const nappePosition = divID.getAttribute('value');
    // On vide la div après avoir récupérer la chaîne
    divID.innerHTML = "";

    // On récupère les valeurs des éléments input
    const trimStringValue = string.replace(/ /g, ""); // On récupère la valeur de la chaîne sans espace
    const splitArray = trimStringValue.split(";"); // On récupère un tableau comprenant toutes les valeurs de la chaîne

    // Initialisation de certaines variables
    let count = 0;
    let beginValue;
    let endValue;
    let countL = 0;
    let countType2 = 0;

    // Sur les jeux de données fournis on peut trouver les valeurs type 2 et type 3 aux index suivants :
    const indexSecond3 = splitArray.indexOf("3", 1);
    const type2Value = splitArray[indexSecond3 + 1];
    const type3Value = splitArray[indexSecond3 + 3];

    // Conditions sur les jeux de données à ce jour pour récupérer première valeur (hors 0) et dernière valeur de la chaîne
    if (splitArray[0] === "2") {
        beginValue = trimStringValue.split(";")[1];
    }
    if (splitArray[0] === "3") {
        beginValue = trimStringValue.split(";")[3];
    }
    endValue = splitArray[splitArray.length - 2];

    // Réarrangement du tableau pour obtenir les différentes longueurs de la chaîne
    if (splitArray[0] === "3") {
        indexOfZero = splitArray.indexOf("0");
        splitArray.splice(indexOfZero, 1);
        splitArray.splice(indexOfZero - 1, 0, "0");
    }

    indexOfBegin = splitArray.indexOf(beginValue);
    splitArray.splice(indexOfBegin, 1);
    splitArray.splice(indexOfBegin - 1, 0, beginValue);

    indexOfEnd = splitArray.lastIndexOf(endValue);
    splitArray.splice(indexOfEnd, 1);
    splitArray.splice(indexOfEnd - 1, 0, endValue);

    let indexes = [],
        i;
    for (i = 0; i < splitArray.length; i++)
        if (splitArray[i] === type2Value) indexes.push(i);

    indexes.forEach((index) => {
        splitArray.splice(index, 1);
        splitArray.splice(index - 1, 0, type2Value);
    });

    // On parcourt chaque élément de la chaîne pour déterminer préalablement la longueur totale de la chaîne = 100% de l'écran
    let countTotal = 0;
    splitArray.map(function (item) {
        switch (item) {
            case "L":
                item = LValue;
                countTotal += item;
                break;
            case "":
            case "2":
            case "4":
            case "3":
                break;
            case beginValue:
                item = parseInt(item, 10);
                countTotal += item;
                break;
            case endValue:
                item = parseInt(item, 10);
                countTotal += item;
                break;
            case type2Value:
                item = parseInt(item, 10);
                countTotal += item;
                break;
            default:
                item = parseInt(item, 10);
                countTotal += item;
                break;
        }
    });

    // On parcourt à nouveau chaque élément de la chaîne
    // Pour chaque élément on crée une div
    // La longueur de chaque div est déterminée par calcul grâce à la longueur totale de la chaîne déterminée précédement
    // On vérifie que la valeur du compteur final est inférieure à la valeur maximum autorisée
    if (countTotal > maximumValue) {
        selectNappeString.insertAdjacentHTML("beforeend", '<p class="nappes__item--error-value"><u><strong>Erreur</strong></u> : La valeur de la chaîne est trop importante pour être lue (maximum autorisé : <strong>' + maximumValue + ' cm</strong>)</p>')
    } else {
        splitArray.map(function (item, index) {
            selectNappeString.insertAdjacentHTML(
                "beforeend",
                '<div class="nappes__item nappes__item__' +
                nappePosition +
                "__" +
                index +
                ' "><div class="nappes__item__eboutages"></div><div class="nappes__item__lsequences"></div><div class="nappes__item__length"></div><div class="nappes__item__count"></div></div>'
            );
            const selectItem = document.querySelector(
                ".nappes__item__" + nappePosition + "__" + index
            );
            const selectEboutages = document.querySelector(
                ".nappes__item__" +
                nappePosition +
                "__" +
                index +
                " .nappes__item__eboutages"
            );
            const selectLSequences = document.querySelector(
                ".nappes__item__" +
                nappePosition +
                "__" +
                index +
                " .nappes__item__lsequences"
            );
            const selectLength = document.querySelector(
                ".nappes__item__" + nappePosition + "__" + index + " .nappes__item__length"
            );
            const selectCount = document.querySelector(
                ".nappes__item__" + nappePosition + "__" + index + " .nappes__item__count"
            );

            switch (item) {
                case "L":
                    item = LValue;
                    count += item;
                    countL++; // On compte le nombre de L afin d'afficher la séquence de L désirée sur le dessin
                    selectLSequences.innerHTML = countL;
                    selectItem.style.width =
                        "calc((" + item + " / " + maximumValue + " * 100%)";
                    selectLength.classList.replace(
                        "nappes__item__length",
                        "nappes__item__length--typeL-active"
                    );
                    selectItem.classList.replace(
                        "nappes__item",
                        "nappes__item--typeL-active"
                    );
                    // Utilisation de la librairie tippy pour afficher les Tooltips (#élément de la chaîne, longueur ou type de l'élément)
                    tippy(".nappes__item__" + nappePosition + "__" + index, {
                        content:
                            "<strong># " + (nappePosition + " " + (index + 1)) + "</strong></br>" + item.toLocaleString() + " cm",
                        placement: "bottom",
                        allowHTML: true,
                    });
                    break;
                case "":
                    break;
                case "0":
                    selectItem.style.width = "0";
                    break;
                case "2":
                    countType2++; // Dans les chaînes fournies, le calcul de la valeur d'éboutages en début de chaîne est différente de celle en milieu de chaîne
                    if (countType2 === 1) {
                        // au début on affiche seuelement la valeur de début de chaîne
                        selectEboutages.innerHTML =
                            '<p class="nappes__item__eboutages__begin">(' +
                            parseInt(beginValue, 10) +
                            ")<p>";
                    } else {
                        // En milieu de chaîne c'est la somme du type 2 + type 3 qui est affichée
                        selectEboutages.innerHTML =
                            '<p class="nappes__item__eboutages__middle">(' +
                            (parseInt(type2Value, 10) + parseInt(type3Value, 10)) +
                            ")<p>";
                    }
                    selectItem.classList.replace(
                        "nappes__item",
                        "nappes__item--type2-active"
                    );
                    tippy(".nappes__item__" + nappePosition + "__" + index, {
                        content: "<strong># " + (nappePosition + " " + (index + 1)) + "</strong></br>Type 2",
                        placement: "bottom",
                        allowHTML: true,
                    });
                    break;
                case "4":
                    selectEboutages.innerHTML =
                        '<p class="nappes__item__eboutages__end">(' +
                        (parseInt(type2Value, 10) + parseInt(endValue, 10)) +
                        ")</p>"; // En fin de chaîne c'est la somme du type 2 + valeur de fin qui est affichée
                    selectItem.classList.replace(
                        "nappes__item",
                        "nappes__item--type2-active"
                    );
                    tippy(".nappes__item__" + nappePosition + "__" + index, {
                        content: "<strong># " + (nappePosition + " " + (index + 1)) + "</strong></br>Type 4",
                        placement: "bottom",
                        allowHTML: true,
                    });
                    break;
                case "3":
                    countL = 0;
                    selectLength.classList.replace(
                        "nappes__item__length",
                        "nappes__item__length--type3-active"
                    );
                    selectItem.classList.replace(
                        "nappes__item",
                        "nappes__item--type3-active"
                    );
                    selectCount.innerHTML = count;
                    LSquencesValue = 0;
                    tippy(".nappes__item__" + nappePosition + "__" + index, {
                        content: "<strong># " + (nappePosition + " " + (index+1)) + "</strong></br>Type 3",
                        placement: "bottom",
                        allowHTML: true,
                    });
                    break;
                case beginValue:
                    item = parseInt(item, 10);
                    count += item;
                    if (isNaN(item)) {
                        selectItem.style.width = 0;
                    }
                    else {
                        selectItem.style.width =
                        "calc((" + item + " / " + maximumValue + " * 100%)";
                        tippy(".nappes__item__" + nappePosition + "__" + index, {
                            content:
                                "<strong># " + (nappePosition + " " + (index + 1)) + "</strong></br>" + item.toLocaleString() + " cm",
                            placement: "bottom",
                            allowHTML: true,
                        })
                    };
                    break;
                case endValue:
                    item = parseInt(item, 10);
                    count += item;
                    selectItem.style.width =
                        "calc((" + item + " / " + maximumValue + " * 100%)";
                    tippy(".nappes__item__" + nappePosition + "__" + index, {
                        content:
                            "<strong># " + (nappePosition + " " + (index + 1)) + "</strong></br>" + item.toLocaleString() + " cm",
                        placement: "bottom",
                        allowHTML: true,
                    });
                    break;
                case type2Value:
                    // Dans les jeux de données fournies la valeur du type 2 = la valeur du type 3 => pas de cas qui les différencie dans ce code
                    item = parseInt(item, 10);
                    count += item;
                    selectItem.style.width =
                        "calc((" + item + " / " + maximumValue + " * 100%)";
                    tippy(".nappes__item__" + nappePosition + "__" + index, {
                        content:
                            "<strong># " + (nappePosition + " " + (index + 1)) + "</strong></br>" + item.toLocaleString() + " cm",
                        placement: "bottom",
                        allowHTML: true,
                    });
                    break;
                default:
                    break;
            }
        });

        // On ajoute une div de fin pour afficher le compteur final
        if (!isNaN(countTotal)) {
            selectNappeString.insertAdjacentHTML(
                "beforeend",
                '<div class="nappes__item__finalCount">' + countTotal + "</div>"
            );
        } else {
            selectNappeString.insertAdjacentHTML(
                "beforeend",
                '<div class="nappes__item__finalCount">Chaîne non renseignée</div>'
            );
        }
        if (nappePosition === "Croisiere" && !isNaN(countTotal)) {
            selectNappeString.insertAdjacentHTML(
                "afterbegin",
                '<div class="nappes__item__position">' + nappePosition + ":" + nbNappesCruise + "</div>"
            );
        } else {
            if (!isNaN(countTotal)) {
                selectNappeString.insertAdjacentHTML(
                    "afterbegin",
                    '<div class="nappes__item__position">' + nappePosition + ":1</div>"
                );
            }
            else {
                selectNappeString.insertAdjacentHTML(
                    "afterbegin",
                    '<div class="nappes__item__position"></div>'
                );
            }
        }
    }

    // On ajoute du style au div de chaque dessin pour obtenir l'affichage désirée
    selectNappeString.style.display = "flex";
    selectNappeString.style.alignItems = "center";
    selectNappeString.style.padding = "75px 50px";
    selectNappeString.style.margin = "12.5px 25px 12.5px 25px";
    selectNappeString.style.border = "2px solid black";
    selectNappeString.style.width = "100%";
    selectNappeString.style.backgroundColor = "#7030a0";
}



// Lorsqu'on click sur le bouton "Reverse", un écouteur d'évènement lance la fonction callback getReverse()
function getReverse() {
    let selectLegend = document.querySelector(".details__nappes__legend__direction");
    selectLegend.innerHTML = "<i class=\"fas fa-arrow-left fa-2x\"></i><p>Sens de filage</p>";
    let nappesArray = [];
    const selectStart = document.getElementById("details__nappes__Debut__draw").getAttribute('value');
    const selectCruise = document.getElementById("details__nappes__Croisiere__draw").getAttribute('value');
    const selectEnd = document.getElementById("details__nappes__Fin__draw").getAttribute('value');
    nappesArray.push(selectStart, selectCruise, selectEnd)

    // On veut appliquer la fonction "Reverse" aux 3 dessins en même temps
    // On ajoute du style à certains élément pour obtenir le dessin voulu
    nappesArray.forEach((position) => {
        const selectNappesIndex = document.querySelector("#details__nappes__" + position + "__draw");

        selectNappesIndex.style.flexDirection = "row-reverse";

        const selectAllLColumns = document.querySelectorAll(
            ".nappes__item--typeL-active"
        );
        selectAllLColumns.forEach((element) => {
            element.style.borderLeft = "2px solid black";
            element.style.borderRight = "0px solid black";
        });

        const selectFinalCount = document.querySelectorAll(
            ".nappes__item__finalCount"
        );
        selectFinalCount.forEach((element) => {
            element.style.paddingRight = "30px";
            element.style.paddingLeft = "0";
        });

        const selectEboutagesSum = document.querySelectorAll(
            ".nappes__item__eboutages__middle"
        );
        selectEboutagesSum.forEach((element) => {
            element.style.right = "0";
        });
    });
}



// Lorsqu'on click sur le bouton "Normal", un écouteur d'évènement lance la fonction callback getNormal()
function getNormal() {
    let selectLegend = document.querySelector(".details__nappes__legend__direction");
    selectLegend.innerHTML = "<p>Sens de filage</p><i class=\"fas fa-arrow-right fa-2x\"></i>";
    let nappesArray = [];
    const selectStart = document.getElementById("details__nappes__Debut__draw").getAttribute('value');
    const selectCruise = document.getElementById("details__nappes__Croisiere__draw").getAttribute('value');
    const selectEnd = document.getElementById("details__nappes__Fin__draw").getAttribute('value');
    nappesArray.push(selectStart, selectCruise, selectEnd)

    // On veut appliquer la fonction "Reverse" aux 3 dessins en même temps
    // On ajoute du style à certains élément pour obtenir le dessin voulu
    nappesArray.forEach((position) => {
        const selectNappesIndex = document.querySelector("#details__nappes__" + position + "__draw");

        selectNappesIndex.style.flexDirection = "row";

        const selectAllLColumns = document.querySelectorAll(
            ".nappes__item--typeL-active"
        );
        selectAllLColumns.forEach((element) => {
            element.style.borderRight = "2px solid black";
            element.style.borderLeft = "0px solid black";
        });

        const selectFinalCount = document.querySelectorAll(
            ".nappes__item__finalCount"
        );
        selectFinalCount.forEach((element) => {
            element.style.paddingLeft = "30px";
            element.style.paddingRight = "0";
        });

        const selectEboutagesSum = document.querySelectorAll(
            ".nappes__item__eboutages__middle"
        );
        selectEboutagesSum.forEach((element) => {
            element.style.right = "30px";
        });
    });
}