/* const str =
    "2;740;L;L;L;3;1000;2;1000;L;L;L;3;1000;2;1000;L;L;L;3;1000;2;1000;L;L;L;3;1000;2;1000;L;L;L;3;1000;4;260;";
let splitStr = str.split(";");
const index = splitStr.indexOf('3');
let newsplit = splitStr.slice(0, index);
console.log(newsplit); */


let count = 0;
let countItem = 0;

// On sélectionne les éléments input du DOM désirés
const inputStringValue = document.querySelector('#stringValue');

const inputLValue = document.querySelector('#lValue');
const inputBeginValue = document.querySelector('#beginValue');
const inputType2Value = document.querySelector('#type2Value');
const inputType3Value = document.querySelector('#type3Value');
const inputEndValue = document.querySelector('#endValue');
const inputCountValue = document.querySelector('#countValue');
const inputCountItem = document.querySelector('#countItem');
const selectDraw = document.querySelector('.draw');
const buttonCheck = document.querySelector('button.check');
const buttonsHidden = document.querySelectorAll('button.hidden');

function getString() {
    // On récupère la valeur des éléments input et on supprime les blancs contenus dans la chaîne de caractères
    const stringValue = inputStringValue.value;
    const trimStringValue = stringValue.replace(/ /g, '');
    const LValue = parseInt(inputLValue.value, 10);
    const beginValue = inputBeginValue.value;
    const type2Value = inputType2Value.value;
    const type3Value = inputType3Value.value;
    const endValue = inputEndValue.value;
    let L = 0;

    // On retire de la chaîne les ";" et on parcourt chaque élément de la chaîne
    // Selon leur type, on ajoute leur valeur au compteur
    trimStringValue.split(";").map(function (item, index) {
        selectDraw.insertAdjacentHTML('beforeend', '<div class="col col-' + index + ' "><div class="eboutages"></div><div class="lsequences"></div><div class="high"></div><div class="middle"></div><div class="low"></div><div class="count"></div></div>');
        countItem++;
        const selectDivCol = document.querySelector('.col-' + index);
        const selectEboutages = document.querySelector('.col-' + index + ' .eboutages');
        const selectLSequences = document.querySelector('.col-' + index + ' .lsequences');
        const selectHigh = document.querySelector('.col-' + index + ' .high');
        const selectMiddle = document.querySelector('.col-' + index + ' .middle');
        const selectLow = document.querySelector('.col-' + index + ' .low');
        const selectCount = document.querySelector('.col-' + index + ' .count');
        switch (item) {
            case "L":
                item = LValue;
                count += item;
                selectMiddle.classList.replace("middle", "middle-L-active");
                selectDivCol.classList.replace("col", "col-active");
                /* selectMiddle.innerHTML = '<img src="./assets/L.png" width="20" height="20" />' */
                switch (L) {
                    case 0:
                    case 3:
                        L = 1;
                        selectLSequences.innerHTML = '1';
                        break;
                    case 1:
                        L = 2;
                        selectLSequences.innerHTML = '2';
                        break;
                    case 2:
                        L = 3;
                        selectLSequences.innerHTML = '3';
                        break;
                    default:
                        break;
                }

            case "":
                break;
            case '2':
                selectMiddle.classList.replace("middle", "middle-2-active");
                selectDivCol.classList.replace("col", "col-active");
                break;
            case '3':
                /* selectMiddle.classList.replace("middle", "middle-3-active"); */
                selectDivCol.insertAdjacentHTML('beforebegin', '<img src="./assets/images/block-change-center.png" width="15" height="15" />')
                selectCount.innerHTML = count + parseInt(type3Value, 10);
                break;
            case '4':
                selectMiddle.classList.replace("middle", "middle-4-active");
                selectDivCol.classList.replace("col", "col-active");
                break;
            case beginValue:
                item = parseInt(item, 10);
                count += item;
                selectEboutages.innerHTML = '(' + item + ')';
                break;
            case type2Value:
            case endValue:
                item = parseInt(item, 10);
                count += item;
                console.log(item);
                selectEboutages.innerHTML = '(' + (item + parseInt(type3Value, 10)) + ')';
                break;
            default:
                item = parseInt(item, 10);
                count += item;
        }
    });

    // On ajoute une div de fin pour afficher le compteur final
    selectDraw.insertAdjacentHTML('beforeend', '<div class="finalCount"><strong>' + count + '</strong></div>');
    buttonCheck.style.visibility = "hidden";
    buttonsHidden.forEach(element => {
        element.style.visibility = "visible";
    });

}

function getStringReverse() {
    selectDraw.style.flexDirection = "row-reverse";
}

function getStringNormal() {
    selectDraw.style.flexDirection = "row";
}


// Dessiner avec Canvas
/* let canvas = document.getElementById("canvas");
if (canvas.getContext) {
    var ctx = canvas.getContext("2d");

    ctx.beginPath();
    ctx.moveTo(50, 50);
    ctx.lineTo(100, 100);
    ctx.stroke();
} */
